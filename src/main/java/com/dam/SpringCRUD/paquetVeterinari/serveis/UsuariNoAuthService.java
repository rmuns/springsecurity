/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.serveis;

import com.dam.SpringCRUD.DAO.UsuariDAO;
import com.dam.SpringCRUD.model.Usuari;
import static com.dam.SpringCRUDAuthentication.utils.EncriptadorContrasenya.encriptarContrasenya;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ricard
 */
@Service
public class UsuariNoAuthService {
    @Getter
    private final int numeroUsuariBloqueado = 10;
    
    @Autowired
    private UsuariDAO usuariDAO;
    
    public Usuari loadUserByUsername(String username){
        return usuariDAO.findByUsername(username);
    }
    
    public void registrarEnUsuari(Usuari usuario){
        usuariDAO.save(usuario);
    }
    
    public boolean checkSavePerson(Usuari usuario){
        String nombre = usuario.getUsername();
        Usuari nombreUsuarioExistente = loadUserByUsername(nombre);
        return nombreUsuarioExistente == null;
    }
    
    public void preSavePerson(Usuari usuario) {
        usuario.setPassword(encriptarContrasenya(usuario.getPassword()));
        registrarEnUsuari(usuario);
    }
    
    public List<Usuari> getAllPersons() {
        List<Usuari> usuarios = usuariDAO.findAllUsuaris();
        for(Usuari usuari : usuarios){
            if(usuari.getIntents()!=getNumeroUsuariBloqueado()){
                usuari.setEnabled(true);
            }
        }
        return usuarios;
    }
    
    public void checkSavePerson(Long id){
        Usuari usuari = usuariDAO.findById(id).orElse(null);
        usuari.setIntents(0);
        registrarEnUsuari(usuari);
    }
}
