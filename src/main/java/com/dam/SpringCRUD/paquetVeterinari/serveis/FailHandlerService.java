/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.serveis;

import com.dam.SpringCRUD.model.Usuari;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ricard
 */
@Component
@Slf4j
public class FailHandlerService extends SimpleUrlAuthenticationFailureHandler{
    @Getter
    private final int numeroUsuariBloqueado = 10;
    
    @Autowired
    private UsuariNoAuthService UsuariNoAuthService;
    
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException{
        int numeroMaxIntents = 3;
        String usuariAdministrador = "admin";
        String mensajeDeError = "";
        
        String nombre = request.getParameter("username");
        Usuari usuario = UsuariNoAuthService.loadUserByUsername(nombre);
        //Verifica si l'usuari existeix abans de comptar els intents
        if (usuario != null) {
            log.info("Comprobando error en autentificacion: " + nombre);
            
            Long id = usuario.getIdUsuari();
            int numeroIntents = usuario.getIntents();
            numeroIntents++;
            log.info(id + ". Con numero de intentos: " + numeroIntents);
            
            if (!nombre.equals(usuariAdministrador)) {
                if (controlDeIntentos(usuario, numeroIntents, numeroMaxIntents)) {
                    int intentsRestants = numeroMaxIntents - numeroIntents;
                    mensajeDeError = "Credencials incorrectes. Numero d'intents restants " + intentsRestants;
                } else {
                    mensajeDeError = "Ha esgotat els intents. Contacti amb l'administrador del sistema";
                }
            } else {
                mensajeDeError = "Credencials incorrectes.";
            }
        } else {
            mensajeDeError = "Credencials incorrectes. Registri un nou usuari";
        }
        log.info("Missatge de error: "+mensajeDeError);
        exception = new LockedException(mensajeDeError);
        
        //Donde se dirige la pagina despues del error
        super.setDefaultFailureUrl("/login?error");
        //
        super.onAuthenticationFailure(request, response, exception);
    }
    
    public boolean controlDeIntentos(Usuari usuario, int numeroIntents, int numeroMaxIntents){
        usuario.setIntents(numeroIntents);
        if (numeroIntents < numeroMaxIntents) {
            UsuariNoAuthService.registrarEnUsuari(usuario);
            return true;
        } else if (numeroIntents >= numeroMaxIntents) {
            usuario.setIntents(getNumeroUsuariBloqueado());
            UsuariNoAuthService.registrarEnUsuari(usuario);
        }
        return false;
    }
}