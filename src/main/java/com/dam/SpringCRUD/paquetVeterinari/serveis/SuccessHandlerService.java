/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.serveis;

import com.dam.SpringCRUD.model.Usuari;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ricard
 */
@Component
@Slf4j
public class SuccessHandlerService extends SimpleUrlAuthenticationSuccessHandler{
    @Autowired
    private UsuariNoAuthService UsuariNoAuthService;
    
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String nombre = request.getParameter("username");
        Usuari usuario = UsuariNoAuthService.loadUserByUsername(nombre);
        if (usuario != null) {
            log.info("Comprobando success en autentificacion: " + nombre);
            usuario.setIntents(0);
            UsuariNoAuthService.registrarEnUsuari(usuario);
            
            if(nombre.equals("admin")){
                response.sendRedirect("/portal");
            }
            
            super.onAuthenticationSuccess(request, response, authentication);
        }
    }
}
