/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.controladors;

import com.dam.SpringCRUD.model.Usuari;
import com.dam.SpringCRUD.paquetVeterinari.serveis.UsuariNoAuthService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author Ricard
 */
@Controller
public class UserController {
    @Autowired
    private UsuariNoAuthService usuariNoAuthService;
    
    @GetMapping("/portal")
    public String accessAPortal() {
        return "portal";
    }
    
    @GetMapping("/iniciUsuaris")
    public String llistarUsuaris(Model model) {
        List<Usuari> usuaris = usuariNoAuthService.getAllPersons();
        model.addAttribute("usuaris", usuaris);
        return "llistarUsuaris";
    }
    
    @GetMapping("/enable/{id}")
    public String reactivarUsuari(@PathVariable Long id, Model model){
        usuariNoAuthService.checkSavePerson(id);
        return "redirect:/iniciUsuaris";
    }
}
