/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dam.SpringCRUD.paquetVeterinari.controladors;

import com.dam.SpringCRUD.model.Usuari;
import com.dam.SpringCRUD.paquetVeterinari.serveis.UsuariNoAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Ricard
 */
@Controller
public class SignupController {
    
    @Autowired
    UsuariNoAuthService usuariNoAuthService;
    
    @GetMapping("/signup")
    public String formSignUp() {
        return "/formulariUsuari";
    }
    
    @PostMapping("/signup")
    public String signUp(Usuari usuario, Model model) {
        if (usuariNoAuthService.checkSavePerson(usuario)) {
            usuariNoAuthService.preSavePerson(usuario);
        }
        model.addAttribute("checkClientMessage", true);
        model.addAttribute("clientMessage", "Inicie session con el nuevo usuario: " + usuario.getUsername());
        return "redirect:/login";
    }
}
